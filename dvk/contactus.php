<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>DVK - Projects</title>

        <!-- Icon css link -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/materialdesignicons.min.css" rel="stylesheet">

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="vendors/animate-css/animate.css" rel="stylesheet">
        <link href="vendors/owl-carousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="css/style.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
        input[type=text] {
    border: 1px solid blue;
    border-radius: 4px;
}
input[type=email] {
border: 1px solid blue;
border-radius: 4px;
}
input[type=tel] {
border: 1px solid blue;
border-radius: 4px;
}
textarea[type=text] {
border: 1px solid blue;
border-radius: 4px;
}
        </style>
    </head>
    <body>


        <!--================Header Area =================-->
        <header class="main_header_area">
            <div class="header_top_area">
                <div class="container">
                    <div class="pull-left">
                      <a href="#"><i class="fa fa-phone"></i>9396936969</a>
                      <a href="#"><i class="fa fa-map-marker"></i>Plot no.123, 124, Jayanagar colony, Kukatpally,Hyderabad-72</a>
                      <a href="#"><i class="mdi mdi-clock"></i>08 AM - 10 PM</a>
                    </div>
                    <!-- <div class="pull-right">
                        <ul class="header_social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
            <div class="main_menu_area">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                            <!-- <a class="navbar-brand" href="#"><img src="img/logo-white.png" alt=""><img src="img/logo.png" alt=""></a> -->
                            <a class="navbar-brand" href="index.html" style="color:yellow">DVK Projects</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                 <li class="dropdown submenu ">
                                    <a href="index.html"  role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                                    <!-- <a href="index-2.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                                    <ul class="dropdown-menu"> -->
                                        <!-- <li><a href="index.html">Home 01</a></li> -->
                                        <!-- <li><a href="index-2.html">Home 02</a></li> -->
                                    <!-- </ul> -->
                                </li>
                                <li class="dropdown submenu ">
                                    <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects</a> -->
                                    <a href="project-three-column.html" role="button" aria-haspopup="true" aria-expanded="false">Projects</a>
                                    <!-- <ul class="dropdown-menu"> -->
                                        <!-- <li><a href="project-full-width.html">Project Full Width</a></li>
                                        <li><a href="project-grid-three-column.html">Project Grid 01</a></li>
                                        <li><a href="project-grid-two-column.html">Project Grid 02</a></li> -->
                                        <!-- <li><a href="project-three-column.html">Project Grid 03</a></li> -->
                                        <!-- <li><a href="project-single.html">Single Project</a></li> -->
                                    <!-- </ul> -->
                                </li>
                                <li><a href="service-single.html">Services</a></li>
                                <li><a href="home.html#aboutus_team">about us</a></li>
                                <!-- <li><a href="blog-details.html">blog</a></li> -->
                                <li class="active"><a href="contactus.php">Contact Us</a></li>
                                <!-- <li class="icon_search"><a href="#"><i class="mdi mdi-cart"></i></a></li> -->
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div>
            </div>
        </header>
        <!--================Header Area =================-->



        <!--================Our Project2 Area =================-->
        <section class=" our_project_three_column" style="padding-bottom:10px">
            <div class="main_c_b_title">
                <h2 class=""> We’d love to hear from you</h2>
                <h6>Drop us a message for your queries and we will get back to you. Or you can simply get in touch through phone or email</h6>
            </div>

            <div class="our_project_details">
              <div class="col-md-3"></div>
                <div class="col-md-6 media-container-column building isolation interior "  data-form-type="formoid">

                                      <div data-form-alert="" hidden="">
                                          Thanks for filling out the form!
                                      </div>

                                      <form class="mbr-form" action="email.php" method="post" data-form-title="Mobirise Form"><input type="hidden" data-form-email="true" value="R46ImfT8lRYLhZq6vpVE63L0V35bOdmwHjgn18Tp/HdDujD3JfWe/8bm7YjE2vjZZ7JFQMRNRIinYQUh3aNa2qi1GshxArFhHAffNuiFsZiNLfMCF8KElmS7V7lbqBIL">
                                          <div class="row row-sm-offset">
                                              <div class="col-md-3 multi-horizontal" data-for="name">
                                                  <div class="form-group">
                                                      <label class="form-control-label mbr-fonts-style display-7" for="name-form1-4" >Name</label>
                                                      <input type="text" class="form-control" name="name" data-form-field="Name" placeholder="Enter Your Name" required="" id="name-form1-4">
                                                  </div>
                                              </div>
                                              <div class="col-md-1"></div>
                                              <div class="col-md-3 multi-horizontal" data-for="email" >
                                                  <div class="form-group">
                                                      <label class="form-control-label mbr-fonts-style display-7" for="email-form1-4">Email</label>
                                                      <input type="email" class="form-control" name="email" placeholder="Enter Your Email" data-form-field="Email" required="" id="email-form1-4">
                                                  </div>
                                              </div>
                                              <div class="col-md-1"></div>
                                              <div class="col-md-4 multi-horizontal" data-for="phone">
                                                  <div class="form-group">
                                                      <label class="form-control-label mbr-fonts-style display-7" for="phone-form1-4">Phone</label>
                                                      <input type="tel" class="form-control" placeholder="Enter Your Mobile Number" name="phone" data-form-field="Phone" id="phone-form1-4">
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="form-group" data-for="message">
                                              <label class="form-control-label mbr-fonts-style display-7" for="message-form1-4">Message</label>
                                              <textarea type="text" class="form-control" name="message" rows="7" data-form-field="Message" id="message-form1-4" placeholder="Write us yours requirement"></textarea>
                                          </div>

                                          <span class="input-group-btn"><button href="" type="submit" class="btn btn-form btn-primary">SEND</button></span>
                                      </form>

                </div>


            </div>
        </section>
        <!--================End Our Project2 Area =================-->

        <!--================Get Quote Area =================-->
        <section class="get_quote_area yellow_get_quote">
            <div class="container">
                <div class="pull-left">
                    <h4>Looking for a quality and affordable constructor for your next project?</h4>
                </div>
                <div class="pull-right">
                    <a class="get_btn_black" href="contactus.php">Contact Us</a>
                </div>
            </div>
        </section>
        <!--================End Get Quote Area =================-->

        <!--================Footer Area =================-->
        <footer class="footer_area">
            <div class="footer_widgets_area footer_bg">
                <div class="container">
                    <div class="row footer_widgets_inner">
                        <div class="col-md-4 col-sm-6">
                            <aside class="f_widget about_widget">
                                <!-- <img src="img/footer-logo.png" alt=""> -->
                                <a href="#" style="font-size:20px">DVK Projects</a>
                                <!-- <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan-tium doloremque laudantium. ed quia consequuntur magni dolores eos qui ratione.</p> -->
<p>DVK Projects Pvt Ltd is a professionally managed real estate development company engaged in development of property, promotional of residential apartments, and construction of independent villas. we have many satisfied families to realize their dream homes</p>
                                <!-- <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul> -->
                            </aside>
                        </div>
                        <!-- <div class="col-md-3 col-sm-6">
                            <aside class="f_widget recent_widget">
                                <div class="f_w_title">
                                    <h3>Recent Posts</h3>
                                </div>
                                <div class="recent_w_inner">
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="img/blog/recent-post/recent-1.png" alt="">
                                        </div>
                                        <div class="media-body">
                                            <a href="#"><p>The road to success is always under con-struction</p></a>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="img/blog/recent-post/recent-2.png" alt="">
                                        </div>
                                        <div class="media-body">
                                            <a href="#"><p>The road to success is always under con-struction</p></a>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </div> -->
                        <div class="col-md-4 col-sm-6">
                            <aside class="f_widget address_widget">
                                <div class="f_w_title">
                                    <h3>Office Address</h3>
                                </div>
                                <div class="address_w_inner">
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="media-body">
                                            <p>Plot no.123, 124, Jayanagar colony, Kukatpally,Hyderabad-72</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="media-body">
                                            <p>9396936969</p>
                                            <!-- <p>(012) 3456789 </p> -->
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="media-body">
                                            <p>vamsi.arch@gmail.com</p>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <aside class="f_widget give_us_widget">
                                <h5>Give Us A Call</h5>
                                <h4>9396936969</h4>
                                <h5>or</h5>
                                <a class="get_bg_btn" href="contactus.php">Contact Us</a>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_copy_right">
                <div class="container">
                    <h4><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></h4>
                </div>
            </div>
        </footer>
        <!--================End Footer Area =================-->









        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-2.2.4.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>

        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="vendors/counterup/waypoints.min.js"></script>
        <script src="vendors/counterup/jquery.counterup.min.js"></script>
        <script src="vendors/flex-slider/jquery.flexslider-min.js"></script>

        <!--gmaps Js-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
        <script src="js/gmaps.min.js"></script>

        <script src="js/theme.js"></script>
        <script>
        <?php if(isset($_GET['message'])){ ?>
    $("document").ready( function () {
        alert("Thank You ,Message was sent");
        window.location.href = "contactus.php"
    });
    <?php } ?>
</script>
    </body>
</html>
